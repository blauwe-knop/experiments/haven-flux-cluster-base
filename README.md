# Bootstrap chart

## Development

While you're developing the Helm chart, use the test values to ensure complex settings are being tested.

```sh
helm template . --values=values.test.yaml --debug
```
